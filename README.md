# ChurnJR API

Copyright : 2021, Jerome Assouline, Robin Timsit

=============

# Getting started

## 0. Clone this repository

```
$ git clone <this_project>
$ cd <this_project>
```

## 1. Setup your virtual environment and activate it

Goal : create a local virtual environment in the folder `./.venv/`.

- First: check your python3.8 version:

    ```
    $ python3.8 --version
    # examples of outputs:
    Python 3.8.2 :: Anaconda, Inc.
    Python 3.8.5

    $ which python3.8
    /Users/benjamin/anaconda3/bin/python3
    /usr/bin/python3
    ```

    - If you don't have python3.8 and you are working on your mac: install it from [python.org](https://www.python.org/downloads/)
    - If you don't have python3 and are working on an ubuntu-like system: install from package manager:

        ```
        $ apt-get update
        $ apt-get -y install python3.8 python3.8-pip python3.8-venv
        ```

- Now that python3.8 is installed create your environment, activate it and install necessary packages:

    ```
    $ source init.sh
    ```

    You sould **allways** activate your environment when working on the project.

    If it fails with one of the following message :
    ```
    "ERROR: failed to create the .venv : do it yourself!"
    ```

## 2. API use

- Go to the main page http://35.240.109.175:8000/ 
    * Upload your csv files (order of files matter!)
    * Click "Envoyer" button and see your predictions download in a csv file

- API documentation and unitary churn prediction are available at http://35.240.109.175:8000/docs

## 3. Chaos module documentation

- To generate the module documentation:

    ```
    $ cd docs
    $ make html
    ```
- Explore documentation:
    ```
    $ open build/html/index.html 
    ```

## 4. If you feel like updating source code & settings !

Your code will go in the folder `chaos/`.

You can change your settings (where data is stored, the names of features, the parameters of models...)
in `chaos/settings/`:
    - `settings.py` should contain the configuration and env. variables you can change


# Project Organization 
----------------------

    ├── activate.sh
    ├── init.sh
    ├── README.md          <- The top-level README for users and developers using this project.
    ├── Dockerfile
    │
    ├── data          <- Directory to store data in.
    │
    ├── docs
    │   ├── make.bat
    │   ├── Makefile
    │   ├── source        <- Files to build documentation.
    │   └── build
    │       ├── toctrees
    │       └── html       <- HTML files to see the module documentation.
    │
    │
    ├── deployment           <- YAML files for deployment
    │   ├── deployment.yml       
    │   └── load_balancer.yml                        
    │
    │
    ├── models             <- Trained dummy model for tests.
    │
    │
    │
    ├── requirements.txt            <- Requirements for users.
    │
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so chaos can be imported
    ├── chaos                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes chaos a Python module
    │   │
    │   ├── infrastructure           <- Scripts to load and clean data.
    │   │   ├── raw_bank_data.py
    │   │   ├── bank_data.py
    │   │   ├── connexion.py    
    │   │   ├── tecnical_cleaning.py
    │   │   └── config
    │   │
    │   ├── settings       <- User and developpers settings
    │   │   ├── settings.py
    │   │   └── user_settings.py
    │   │
    │   ├── domain         <- Scripts to clean/create features and train model
    │   │   ├── domain_cleaning.py
    │   │   ├── train_model.py
    │   │   └──feature_engineering.py
    │   │
    │   ├── application     <- Churn JR App module
    │   │   ├── server_fastapi.py
    │   │   ├── server_flask.py
    │   │   ├── prediction_fastapi.py
    │   │   └── prediction_flask.py
    │   │
    │   ├── interface       <- HTML tamplates
    │   │   └── templates
    │   │
    │   ├── test        <- Unit and functional tests
    │   │   ├── functional
    │   │   └── unit


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
