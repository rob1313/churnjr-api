.. _predictmodelmodule: predictmodel

Module predict_model
********************
This document describes the module predict_model.

.. automodule:: chaos.application.predict_model

============================================================
Functions
============================================================

.. autofunction:: chaos.application.predict_model.import_model()
.. autofunction:: chaos.application.predict_model.predict_churn()