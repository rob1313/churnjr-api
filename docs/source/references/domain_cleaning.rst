.. _domaincleaningmodule: domaincleaning

Module domain_cleaning
**********************
This document describes all classes of the module domain_cleaning.

.. automodule:: chaos.domain.domain_cleaning

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.FrequencyEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.FrequencyEncoder.transform()
    .. automethod:: chaos.domain.domain_cleaning.FrequencyEncoder.fit()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.CustomOneHotEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.CustomOneHotEncoder.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.AberrantAgeImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.AberrantAgeImputer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.AberrantNbProduitsImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.AberrantNbProduitsImputer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.CreditScoreImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.CreditScoreImputer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.SalaryImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.SalaryImputer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.domain_cleaning.BalanceImputer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.domain_cleaning.BalanceImputer.transform()
    .. automethod:: chaos.domain.domain_cleaning.BalanceImputer.fit()
