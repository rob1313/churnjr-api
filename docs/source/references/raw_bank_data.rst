.. _rawbankdatamodule: rawbankdata

Module raw_bank_data
********************
This document describes all classes of the module raw_bank_data.

.. automodule:: chaos.infrastructure.raw_bank_data

============================================================
    Class
============================================================

.. autoclass:: chaos.infrastructure.raw_bank_data.RawBankData()

************************************************************
    Properties
************************************************************

    .. autoproperty:: chaos.infrastructure.raw_bank_data.RawBankData.data()
    .. autoproperty:: chaos.infrastructure.raw_bank_data.RawBankData.features()
    .. autoproperty:: chaos.infrastructure.raw_bank_data.RawBankData.target()
