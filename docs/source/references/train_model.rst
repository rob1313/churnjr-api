.. _trainmodelmodule: trainmodel

Module train_model
******************
This document describes all functions of the module train_model.

.. automodule:: chaos.domain.train_model

============================================================
Functions
============================================================

.. autofunction:: chaos.domain.train_model.train_model()
.. autofunction:: chaos.domain.train_model.evaluate_model()
