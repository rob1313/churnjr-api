FROM python:3.8.5

COPY . /chaos-1
WORKDIR /chaos-1

RUN apt-get update

RUN pip install -r requirements.txt
RUN pip install -e ./


ENV FASTAPI_PORT=8000
EXPOSE ${FASTAPI_PORT}

CMD ["uvicorn", "chaos.application.server_fastapi:app", "--host", "0.0.0.0", "--port", "8000"]