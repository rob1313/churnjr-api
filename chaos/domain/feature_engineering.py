"""This module provides classes to build new columns.

In a pipeline, classes ending with Creator should be used after classes
ending with Imputer from domain_cleaning module.

Classes
-------
SeniorityCreator

AgeClassCreator

CreditScoreAgeRatioCreator

BalanceWageRatioCreator

CreditScoreByNProductsCreator

FeatureDropper
"""


import os
import sys
import datetime
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin

import chaos.settings.settings as stg


class SeniorityCreator(BaseEstimator, TransformerMixin):
    """Create a column of seniority.

    Attributes
    ----------
    column_name: str
        Name of the column to create.

    """

    def __init__(self, column_name):
        """Initialize class.

        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Add a column of integers being the number of days since they became customers.

        Arguments
        ---------
        X: pandas.DataFrame
            Cleaned dataframe.

        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[self.column_name] = X_[stg.DATE_ENTREE].apply(lambda x: (stg.EXTRACT_DATE - x).days)
        return X_

class AgeClassCreator(BaseEstimator, TransformerMixin):
    """Create a column of age classes.
    
    Attributes
    ----------
    column_name: str
        Name of the column to create.
    
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create column of age classes.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Cleaned dataframe with imputed aberrant ages.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[self.column_name] = pd.cut(x=X_[stg.AGE], bins=stg.CATEGORIES_AGE_DECOUPAGE, labels=stg.NOMS_CATEGORIES_AGE)
        return X_

class CreditScoreAgeRatioCreator(BaseEstimator, TransformerMixin):
    """Create a column with credit score over age ratio.
    
    Attributes
    ----------
    column_name: str
        Name of the column to create.

    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with credit score by age ratio.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Cleaned dataframe with imputed credit scores and ages.
            Age should never be 0 in X.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[self.column_name] = round(X_[stg.SCORE_CREDIT]/X_[stg.AGE], 2)
        return X_

class BalanceWageRatioCreator(BaseEstimator, TransformerMixin):
    """Create a column with balance over wage ratio.
    
    Attributes
    ----------
    column_name: str
        Name of the column to create.
    
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with balance by wage ratio.
        Affect to outliers the max of the rest of the distribution.

        Arguments
        ---------
        X: pandas.DataFrame
            Cleaned dataframe with imputed salaries and balances.
            Salary should never be 0 in X.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[self.column_name] = X_[stg.BALANCE]/X_[stg.SALAIRE]
        
        ratio_max = X_.loc[X_[self.column_name]<1000, self.column_name].max()
        X_.loc[X_[self.column_name]>1000, self.column_name] = ratio_max

        return X_

class CreditScoreByNProductsCreator(BaseEstimator, TransformerMixin):
    """Create a column with credit score over number of products ratio.
    
    Attributes
    ----------
    column_name: str
        Name of the column to create.
    
    """

    def __init__(self, column_name):
        """Initialize class.
        
        Arguments
        ---------
        column_name: str
        """
        self.column_name = column_name

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Create a column with credit score by number of products ratio.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Cleaned dataframe with imputed credit scores and aberrant number of products.
            Number of products should never be 0 in X.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[self.column_name] = round(X_[stg.SCORE_CREDIT]/X_[stg.NB_PRODUITS], 2)
        return X_

class FeatureDropper(BaseEstimator, TransformerMixin):
    """Drop columns to a pandas.DataFrame.

    Attributes
    ----------
    columns: list
        Name of columns to drop.

    """

    def __init__(self, columns):
        """Initialize class.

        Arguments
        ---------
        columns: list
        """
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Drop columns.

        Arguments
        ---------
        X: pandas.DataFrame
            Any dataframe.

        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_ = X_.drop(columns=self.columns)
        return X_
