"""This module loads data from two flat files and join them.

Classes
-------
RawBankData

"""

import os
import sys
import random
import pandas as pd

import chaos.settings.settings as stg


class BankData:
    """Raw Bank data files and associated pandas DataFrames.
    customers_file and indicators_file media type should be csv.

    Arguments
    ----------
    customers_file: str or file-like object
        Name of the file with customers database.
    customers_sep: str
        Separator for customers database.
    indicators_file: str or file-like object
        Name of the file with indicators database.
    indicators_sep: str
        Separator for indicators database.
    remove_nan: bool
        Whether to remove nan values or not.

    Attributes
    ----------
    customers: pandas.DataFrame
        Dataframe with customers informations.
    indicators: pandas.DataFrame
        Dataframe with indicators informations.
    index_removed: Index
        Indices of removed rows if remove_nan is True.
    dataframe_with_nan: pandas.DataFrame
        Dataframe with rows containing nan values.

    """

    def __init__(self, customers_file, indicators_file,\
                customers_sep=stg.CUSTOMERS_SEP, indicators_sep=stg.INDICATORS_SEP,\
                remove_nan=True):
        """Class initialization.
        
        Arguments
        ---------
        customers_file: str or file-like object
        customers_sep: str
        indicators_file: str or file-like object
        indicators_sep: str
        remove_nan: boolean
        """
        if type(customers_file) != str:
            customers_file.seek(0)
        self.customers = self._import_customers_data(customers_file, customers_sep)
        if type(indicators_file) != str:
            indicators_file.seek(0)
        self.indicators = self._import_indicators_data(indicators_file, indicators_sep)
        self.remove_nan = remove_nan
        self.index_removed = None
        self.dataframe_with_nan = None

    @property
    def data(self):
        """Return full dataframe from files, with target if target exists in indicators file.
        Encode values of target in {0,1}.

        Returns
        -------
        data: pandas.DataFrame
            Merged DataFrame with features (and possibly target).
        """
        joined_df = self._merge_datasets().copy()

        if self.remove_nan:
            joined_df = self._remove_nan(joined_df)

        if stg.CHURN in joined_df.columns:
            joined_df[stg.CHURN] = joined_df[stg.CHURN].map(stg.MAPPING[stg.CHURN])
            data = joined_df
        else:
            data = joined_df
        return data
    
    @property
    def features(self):
        """Return features dataframe from files with no target.
        
        Returns
        -------
        features: pandas.DataFrame
            Dataframe with only features.
        """
        joined_df = self.data.copy()
        if stg.CHURN in joined_df.columns:
            features = joined_df.drop(columns=[stg.CHURN])
            return features
        else:
            features = joined_df
            return features

    @property
    def target(self):
        """Return target column from file (if exists in file).
        
        Returns
        -------
        target: pandas.core.series.Series
            Series with only targets.
        """
        joined_df = self.data.copy()
        if stg.CHURN in joined_df.columns:
            target = joined_df[stg.CHURN]
            return target
        else:
            return None

    def _merge_datasets(self):
        """Merge customers and indicators data into a single dataframe.
        
        Returns
        -------
        joined_df: pandas.DataFrame
        """
        joined_df = pd.merge(self.customers, self.indicators, how='outer', on=stg.CUSTOMERS_ID_CLIENT)
        return joined_df

    def _remove_nan(self, X):
        """Remove missing values from DataFrame.
        
        Arguments
        ---------
        X: pandas.DataFrame

        Returns
        -------
        X_: pandas.DataFrame
        """
        X_ = X.copy()
        
        X_filtered = X_.filter(items=stg.MANDATORY_FIELDS)
        self.index_removed = X_filtered.index[X_filtered.isnull().any(axis=1)]
        
        self.dataframe_with_nan = X_.iloc[self.index_removed]
        
        X_ = X_.dropna(subset=stg.MANDATORY_FIELDS)
        return X_

    @staticmethod
    def _import_customers_data(file, separator):
        """Import customers data from file.

        Returns
        -------
        customers: pandas.DataFrame
        """
        try :
            if type(file) != str:
                file.seek(0)
            customers = pd.read_csv(file, sep=separator, header=0, usecols=stg.CUSTOMERS_COLUMNS,
                                    dtype={stg.CUSTOMERS_ID_CLIENT: str})
        except FileNotFoundError as error:
            print(error)
            print("!! Customers File Not Found !!\n")
        except FileExistsError:
            raise FileExistsError('File extension must be .csv')

        return customers

    @staticmethod
    def _import_indicators_data(file, separator):
        """Imports indicators data from file.

        Returns
        -------
        indicators: pandas.DataFrame
        """
        try:
            if type(file) != str:
                file.seek(0)
            indicators = pd.read_csv(file, sep=separator, header=0, usecols=stg.INDICATORS_COLUMNS,
                                     dtype={stg.INDICATORS_ID_CLIENT: str})
        except ValueError:
            if type(file) != str:
                file.seek(0)
            indicators = pd.read_csv(file, sep=separator, header=0,
                                        usecols=stg.UNLABELED_INDICATORS_COLUMNS,
                                        dtype={stg.INDICATORS_ID_CLIENT: str})
        except FileNotFoundError as error:
            print(error)
            print("!! Indicators File Not Found !!\n")
        except FileExistsError:
            raise FileExistsError('File extension must be .csv')
        
        return indicators
