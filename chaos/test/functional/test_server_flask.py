import requests
import pytest
import numpy as np
import subprocess

from chaos.infrastructure.config.config import config

port = config["api_flask"]["port"]
host = config["api_flask"]["host"]

data = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
        "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
        "CARTE_CREDIT":"No","SALAIRE":10540.31,"SCORE_CREDIT":430.0}

data_w_nan = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
              "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":np.nan,"NB_PRODUITS":2,
              "CARTE_CREDIT":"No","SALAIRE":np.nan,"SCORE_CREDIT":np.nan}

data_missing_value = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
                      "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
                      "CARTE_CREDIT":np.nan,"SALAIRE":10540.31,"SCORE_CREDIT":430.0}

data_missing_field = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00",
                      "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
                      "CARTE_CREDIT":"No","SALAIRE":10540.31,"SCORE_CREDIT":430.0}

@pytest.mark.parametrize('data', [(data,), (data_w_nan,)])
def test_predict_json(data):
    endpoint = "predict-json"
    url = f"http://{host}:{port}/{endpoint}"
    headers = {"Content-type": "application/json"}

    response = requests.get(url=url, json=data, headers=headers)
    results = response.json()
    
    assert results["churn"] in [0,1]
    assert results["churn_proba"] >= 0
    assert results["churn_proba"] <= 1


@pytest.mark.parametrize('data', [(data_missing_value,), (data_missing_field,)])
def test_predict_json_missing_value(data):
    endpoint = "predict-json"    
    url = f"http://{host}:{port}/{endpoint}"
    headers = {"Content-type": "application/json"}

    response = requests.get(url=url, json=data, headers=headers)
    results = response.json()
    
    assert results["churn"] != results["churn"]
    assert results["churn_proba"] != results["churn_proba"]
