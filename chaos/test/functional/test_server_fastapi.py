import requests
import pytest
import numpy as np
import subprocess
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

from chaos.application.server_fastapi import Features, app
from chaos.infrastructure.config.config import config


client = TestClient(app)

port = config["api_flask"]["port"]

LOAD_BALANCER_URL = subprocess.check_output("kubectl describe svc loadbalancer-1-staging \
                                            | grep -E 'LoadBalancer Ingress:' \
                                            | awk '{print $3}'", shell=True)
host = LOAD_BALANCER_URL.decode("utf-8")[:-1]

data = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
        "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
        "CARTE_CREDIT":"No","SALAIRE":10540.31,"SCORE_CREDIT":430.0}

data_w_nan = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
              "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":float("NaN"),"NB_PRODUITS":2,
              "CARTE_CREDIT":"No","SALAIRE":float("NaN"),"SCORE_CREDIT":float("NaN")}

data_missing_value = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00","PAYS":"France",
                      "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
                      "CARTE_CREDIT":None,"SALAIRE":10540.31,"SCORE_CREDIT":430.0}

data_missing_field = {"ID_CLIENT":"12345678","DATE_ENTREE":"2012-04-01 00:00:00",
                      "SEXE":"F","AGE":43,"MEMBRE_ACTIF":"Yes","BALANCE":132148.70,"NB_PRODUITS":2,
                      "CARTE_CREDIT":"No","SALAIRE":10540.31,"SCORE_CREDIT":430.0}

@pytest.mark.parametrize('data', [data, data_w_nan])
def test_predict_json(data):
    endpoint = "predict-json"
    response = client.post(f"http://{host}:{port}/{endpoint}/", json=data)
    results = response.json()
    
    assert results["churn"] in [0,1]
    assert results["churn_proba"] >= 0
    assert results["churn_proba"] <= 1
