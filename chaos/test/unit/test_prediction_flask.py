"""Unit tests of the functions in chaos/application/predict_model.py.
In it, the function predict_churn is the heart of the project so we very carefully test it.
"""

import os
import pathlib

import pytest
import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression

import chaos.settings.settings as stg

from chaos.application.prediction_flask import import_model, predict_churn


customers_test = pd.DataFrame(data={
    'ID_CLIENT': ["15698028"], 'DATE_ENTREE': ["2019-02-01 00:00:00"],
    'NOM': ['Random'], 'PAYS': ['France'], 'SEXE': ['F'], 'AGE': [41],
    'MEMBRE_ACTIF': ['No']
    })

customers_test_several = customers_test_unit = pd.DataFrame(data={
    'ID_CLIENT': ["15698028", "01234567"], 'DATE_ENTREE': ["2019-02-01 00:00:00", "2018-04-01 00:00:00"],
    'NOM': ['Random', 'Guy'], 'PAYS': ['France', 'Allemagne'], 'SEXE': ['F', 'H'], 'AGE': [41, 114],
    'MEMBRE_ACTIF': ['No', 'No']
    })


indicators_test = pd.DataFrame(data={
    'ID_CLIENT': ['15698028'], 'BALANCE': [0.0], 'NB_PRODUITS': [2],
    'CARTE_CREDIT': ['Yes'], 'SALAIRE': [31766.3], 'SCORE_CREDIT': [np.nan]
})

indicators_test_several = pd.DataFrame(data={
    'ID_CLIENT': ['15698028', '123456789'], 'BALANCE': [0.0, 50.0], 'NB_PRODUITS': [2, 1],
    'CARTE_CREDIT': ['Yes', 'Yes'], 'SALAIRE': [31766.3, 21303.0], 'SCORE_CREDIT': [np.nan, 350.0]
})

# Test of the model import
# Test to remove once the storage of the model and databases handled
def test_import_model_file_exists():
    model_filename = stg.MODEL_DUMMY_FILENAME
    assert os.path.isfile(model_filename)

# Test model import
def test_import_model_output():
    model = import_model(stg.MODEL_DUMMY_FILENAME)
    pipe = Pipeline(steps=[("log_reg", LogisticRegression())])
    assert type(model) == type(pipe)

# Tests of the predict_churn function

# Test of file creation
def test_predict_churn_file_creation ():
    indicators_test.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers_test.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    predict_churn('customers.csv', 'indicators.csv', stg.MODEL_DUMMY_FILENAME)
    predictions_path = stg.UPLOAD_PREDICTIONS_FILENAME
    os.remove('indicators.csv')
    os.remove('customers.csv')

    assert os.path.isfile(predictions_path)
    os.remove(stg.UPLOAD_PREDICTIONS_FILENAME)

# Test of number of rows in predictions file
@pytest.mark.parametrize('customers,indicators,expected',
                         [(customers_test, indicators_test_several, 2),
                          (customers_test_several, indicators_test_several, 3)])
def test_predict_churn_n_rows(customers, indicators, expected):
    indicators.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    predict_churn('customers.csv', 'indicators.csv', stg.MODEL_DUMMY_FILENAME)
    predictions_path = stg.UPLOAD_PREDICTIONS_FILENAME
    os.remove('indicators.csv')
    os.remove('customers.csv')

    predictions = pd.read_csv(stg.UPLOAD_PREDICTIONS_FILENAME)
    os.remove(stg.UPLOAD_PREDICTIONS_FILENAME)

    assert len(predictions) == expected

# Test of columns in predictions file
@pytest.mark.parametrize('customers,indicators',
                         [(customers_test, indicators_test_several),
                          (customers_test_several, indicators_test_several)])
def test_predict_churn_columns(customers, indicators):
    indicators.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    predict_churn('customers.csv', 'indicators.csv', stg.MODEL_DUMMY_FILENAME)
    predictions_path = stg.UPLOAD_PREDICTIONS_FILENAME
    os.remove('indicators.csv')
    os.remove('customers.csv')

    predictions = pd.read_csv(stg.UPLOAD_PREDICTIONS_FILENAME)
    os.remove(stg.UPLOAD_PREDICTIONS_FILENAME)

    assert stg.CHURN_PREDICTION in predictions.columns
    assert stg.NOM not in predictions.columns


# Test of values in predictions column
@pytest.mark.parametrize('customers,indicators,expected',
                         [(customers_test, indicators_test_several, 1),
                          (customers_test_several, indicators_test_several, 2)])
def test_predict_churn_output(customers, indicators, expected):
    indicators.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    predict_churn('customers.csv', 'indicators.csv', stg.MODEL_DUMMY_FILENAME)
    predictions_path = stg.UPLOAD_PREDICTIONS_FILENAME
    os.remove('indicators.csv')
    os.remove('customers.csv')

    predictions = pd.read_csv(stg.UPLOAD_PREDICTIONS_FILENAME)
    os.remove(stg.UPLOAD_PREDICTIONS_FILENAME)

    assert len(predictions.query('CHURN_PREDICTION != CHURN_PREDICTION')) == expected
    assert len(predictions.query('CHURN_PREDICTION in ["Yes", "No"]')) == 1

      