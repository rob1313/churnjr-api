import os
import pathlib
import io

import datetime
import pickle
import numpy as np
import pandas as pd

import uvicorn
from fastapi import FastAPI, File, UploadFile, Form, Request, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import HTMLResponse, FileResponse, StreamingResponse, Response
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel, Field
from typing import Optional, List

import chaos.settings.settings as stg
from chaos.infrastructure.bank_data import BankData
from chaos.application.prediction_fastapi import import_model_bucket, predict_churn, import_model


tags_metadata = [
    {
        "name": "example",
        "description": "Empty example",
    },
    {
        "name": "churn",
        "externalDocs": {
            "description": "Git Repository =>",
            "url": "https://gitlab.com/yotta-academy/mle-bootcamp/projects/ml-prod-projects/fall-2020/chaos-1.git",
        },
    },
]

app = FastAPI(
    title="ChurnJR",
    description="This fancy project can predict a potential churner in Yotta Bank International",
    version="1.0.0",
    openapi_tags=tags_metadata
)

templates = Jinja2Templates(directory=stg.TEMPLATES_FOLDER)

class Features(BaseModel):
    # MANDATORY_FIELDS = [DATE_ENTREE, PAYS, SEXE, AGE, MEMBRE_ACTIF, NB_PRODUITS, CARTE_CREDIT]
    ID_CLIENT: Optional[str] = Field(None, example="15791700")
    DATE_ENTREE: str = Field(..., example="2018-01-01 00:00:00", description="datetime format YYYY-MM-DD HH:MM:SS")
    PAYS: str = Field(..., example="Allemagne")
    SEXE: str = Field(..., example="H", description="H/F")
    AGE: int = Field(..., example=47, lt=100, description="between 0 and 100")
    MEMBRE_ACTIF: str = Field(..., example="Yes", description="Yes/No")
    BALANCE: Optional[float] = Field(None, example=118079.47)
    NB_PRODUITS: int = Field(..., example=4)
    CARTE_CREDIT: str = Field(..., example="Yes", description="Yes/No")
    SALAIRE: Optional[float] = Field(None, example=143007.49)
    SCORE_CREDIT: Optional[float] = Field(None, example=773.0)

@app.post("/predict-json/", tags=["churn"])
def predict_json(feature: Features):
    try:
        data = jsonable_encoder(feature)
        # Format into pandas DataFrame
        X = pd.json_normalize(data=data)
        # Import model
        model = import_model_bucket()
        # Predict Churn
        y_proba = model.predict_proba(X)[:, 1]
        y_pred = (y_proba >= stg.THRESHOLD).astype(int)
        # Results
        answer = int(y_pred[0])
        answer_proba = float(y_proba[0])

    except (ValueError, TypeError, KeyError):
        print("====Error in the PREDICTION====")
        DEFAULT_RESPONSE = "nopred"
        answer = DEFAULT_RESPONSE
        answer_proba = DEFAULT_RESPONSE
    
    response = {"churn":answer, "churn_proba":answer_proba}
    return response

@app.post("/predict-csv/", tags=["churn"])
async def predict_csv(
    customers_file: UploadFile = File(...),
    indicators_file: UploadFile = File(...)
    ):
    # Predict Churn
    X = predict_churn(customers_file.file, indicators_file.file)

    # Download predictions
    response = StreamingResponse(
        content=io.StringIO(X.to_csv(index=False)),
        media_type="csv",
        headers={'Content-Disposition': 'attachment; filename="predictions.csv"'}
    )
    return response

@app.get("/", tags=["example"])
async def main(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

if __name__ == "__main__":
    
    from chaos.infrastructure.config.config import config
    
    PORT = config["api_fastapi"]["port"]
    HOST = config["api_fastapi"]["host"]

    print("starting API at", datetime.datetime.now())
    uvicorn.run(app, host=HOST, port=PORT, debug=True)
