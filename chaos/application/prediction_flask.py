"""This module provides functions to make churn predictions.

Functions
---------
predict_churn

import_model

"""

from os import path
import sys

import pickle

import pandas as pd
import numpy as np

from google.cloud import storage, exceptions

import chaos.settings.settings as stg
import chaos.settings.user_settings as ustg

from chaos.infrastructure.raw_bank_data import RawBankData
from chaos.domain.train_model import train_model

def predict_churn(customers_filepath, indicators_filepath, model_filename):
    """Create csv file with churn predictions.
    
    Arguments
    ---------
    customers_filepath: str
        Path to customers.csv file
    indicators_filepath: str
        Path to indicators.csv file
    
    Returns
    -------
    None
    """
    # Import Data
    bank_datatest = RawBankData(
        customers_filename=customers_filepath, customers_sep=ustg.CUSTOMERS_TEST_SEPARATOR,
        indicators_filename=indicators_filepath, indicators_sep=ustg.INDICATORS_TEST_SEPARATOR)
    X_test = bank_datatest.data

    # Import model
    try:
        model = import_model(model_filename)
    except FileNotFoundError as error:
        print("==> Model not found locally. Checking in remote bucket")
        print("==> Error type: ", error)
        try:
            model = import_model_bucket()
        except exceptions.NotFound as error:
            print("==> WARNING Model not found in bucket.")
            print("==> Error type: ", error)

    # Predict
    y_proba = model.predict_proba(X_test)[:, 1]
    y_pred = (y_proba >= stg.THRESHOLD).astype(int)

    # Create output dataframe
    X_test[stg.CHURN_PREDICTION] = y_pred
    X_test[stg.CHURN_PREDICTION] = X_test[stg.CHURN_PREDICTION].map(stg.MAPPING[stg.CHURN_PREDICTION])
    if bank_datatest.dataframe_with_nan.empty == False:
        bank_datatest.dataframe_with_nan[stg.CHURN_PREDICTION] = np.nan
        X_test = pd.concat([X_test, bank_datatest.dataframe_with_nan], axis=0)

    # Save output in csv
    X_test.to_csv(stg.UPLOAD_PREDICTIONS_FILENAME, index=False)

def import_model(file_path=stg.MODEL_FILENAME):
    """Import pickle model already trained.
    
    Arguments
    ---------
    file_path: str
        Path to the trained model.

    Return
    ------
    model: scikit-learn Pipeline
        Fitted pipeline.
    """
    with open(file_path, 'rb') as handle:
        data_model_dict = pickle.load(handle)
        model = data_model_dict['model']
    
    return model

def import_model_bucket(bucket_name=stg.BUCKET_NAME, source_blob_name=stg.MODEL_GS_FILENAME):
    """Import pickle model already trained form bucket.
    
    Arguments
    ---------
    bucket_name: str
        Bucket name.
    source_blob_name: str
        Path to the file trained model in bucket.

    Return
    ------
    model: scikit-learn Pipeline
        Fitted pipeline.
    """
    # Google Storage Client
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    pickle_in = blob.download_as_bytes()
    # Model as pickle
    data_model_dict = pickle.loads(pickle_in)
    model = data_model_dict['model']
    
    return model
