import os
import pathlib

import datetime
import pickle
import numpy as np
import pandas as pd
from collections import OrderedDict

from google.cloud import exceptions

from flask import Flask, jsonify, request, render_template, json, send_file
from werkzeug.utils import secure_filename

import chaos.settings.settings as stg
from chaos.application.prediction_flask import predict_churn, import_model, import_model_bucket

# Change path to access to templates renderer
template_folder_path = pathlib.Path(__file__).resolve().parent.parent / 'interface/templates'

app = Flask(__name__, template_folder=template_folder_path)
app.config['JSON_SORT_KEYS'] = False

@app.route('/')
def welcome():
    return render_template("welcome.html")

@app.route("/predict-json", methods=["GET"])
def predict_json():
    try:
        req_data = request.get_json()
        # Format into pandas DataFrame
        X = pd.json_normalize(data=req_data)
        # Import model from bucket
        try:
            model = import_model_bucket()
        except exceptions.NotFound as error:
            print("==> WARNING Model not found in bucket.")
            print("==> Error type: ", error)
        # Prediction
        y_proba = model.predict_proba(X)[:, 1]
        y_pred = (y_proba >= stg.THRESHOLD).astype(int)
        # Results
        answer = int(y_pred[0])
        answer_proba = float(y_proba[0])
    
    except (ValueError, TypeError, KeyError):
        print("====Error in the PREDICTION====")
        DEFAULT_RESPONSE = float("NaN")
        answer = DEFAULT_RESPONSE
        answer_proba = DEFAULT_RESPONSE
    
    response = {"churn":answer, "churn_proba":answer_proba}
    
    return jsonify(response)

@app.route('/predict-csv')
def predict_csv():
    return render_template("index.html", message="Upload")

@app.route("/predictions", methods=["GET", "POST"])
def predictions():

    if request.method=="POST":
        try:
            # Customers path
            customers = request.files['customers_file']
            customers_filename = secure_filename(customers.filename)
            customers_path = pathlib.PurePath(stg.UPLOAD_SAVE_DIR).joinpath(customers_filename)
            try:
                customers.save(customers_path)
            except FileNotFoundError:
                os.mkdir(stg.UPLOAD_SAVE_DIR)
                customers.save(customers_path)
            # Indicators path
            indicators = request.files['indicators_file']
            indicators_filename = secure_filename(indicators.filename)
            indicators_path = pathlib.PurePath(stg.UPLOAD_SAVE_DIR).joinpath(indicators_filename)
            indicators.save(indicators_path)
            
            # Predict Churn
            predict_churn(customers_path, indicators_path, stg.MODEL_FILENAME)

            return send_file(stg.UPLOAD_PREDICTIONS_FILENAME, as_attachment=True)

        except IsADirectoryError as error:
            print("==> Handling error: ", error)
            return render_template("index.html", message="Needs both files")

        finally:
            os.remove(stg.UPLOAD_PREDICTIONS_FILENAME)
            os.remove(customers_path)
            os.remove(indicators_path)
    
    return render_template("index.html", message="Upload")


if __name__ == "__main__":
    
    from chaos.infrastructure.config.config import config

    PORT = config["api_flask"]["port"]
    HOST = config["api_flask"]["host"]

    print("starting API at", datetime.datetime.now())
    app.run(debug=False, host=HOST, port=PORT)
